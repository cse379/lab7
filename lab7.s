	AREA interrupts, CODE, READWRITE
	EXPORT lab7
	EXPORT FIQ_Handler
	EXTERN uart_init
	EXTERN read_character
	EXTERN pin_connect_block_setup_for_uart0
	EXTERN output_character
	EXTERN output_string
	EXTERN enter
	EXTERN div_and_mod
	EXTERN make_chars_2
	EXTERN illuminateLEDs
	EXTERN display_digit_on_7_seg
	EXTERN Illuminate_RGB_LED
	

prompt = "\n\r\n\r############################################################################################\n\r##                                                                                        ##\n\r##       Welcome to Wee Dig Dug! This is a mini-version of the popular game Dig Dug.      ##\n\r##                                                                                        ##\n\r############################################################################################\n\r\n\r",0
	ALIGN	
score_label = "Score: ",0
	ALIGN
instructions = "RULES OF THE GAME:\n\r 		                                                                 ___________________________\n\rAt the beginning of the game, the player's position is             |	         LEGEND           |\n\rinitialized to the center of the dirt facing down, and the enemy   |                           |\n\rpositions are random (not including the player's position).        | Z   : Unbreakable Wall    |\n\rEach enemy is placed inside of a row or column of three            | #   : Dirt                |\n\rempty spaces (tunnels).  There are two slow enemies and one        | ^   : Player facing UP    |\n\rfast enemy.                                                        | <   : Player facing LEFT  |\n\rThe player initially has four lives and a score of 0.              | >   : Player facing RIGHT |\n\rInitially the movement period of the player is 0.5 seconds         | v   : Player facing DOWN  |\n\r(i.e., the player can move once per half second).                  | b   : Slow enemy          |\n\rA player can move through empty spaces (tunnels) and dirt.         | B   : Fast enemy          |\n\rA player cannot move through an unbreakable wall.                  | *** : Air Pump            |\n\rEnemies can only move through tunnels, not through dirt            |___________________________|\n\rand not through an unbreakable wall.\n\rThe fast enemy moves at the same rate as the player.\n\rThe slow enemies move at half the speed of the fast enemy.\n\rIf head of the air pump makes contact with an enemy,\n\rthe enemy is instantly killed. Multiple enemies can be killed by\n\ra single deployment of the air pump. If the player and\n\rone or more enemies share the same position, the player\n\rloses a life and player and enemies return to initial\n\rpositions.\n\rThe game ends when the player loses all four lives or\n\rafter 120 seconds have elapsed since the start of\n\rgameplay (not including time spent at the main menu or\n\rwhile paused).\n\rIf all enemies are killed, the game advances to the next\n\rlevel, and the player movement period decreases by 0.1\n\rseconds, down to a minimum movement period of 0.1 seconds.\n\rWhen the game advances to the next level, two new slow\n\renemies and 1 fast enemy are spawned with new random\n\rpositions as at the start of the game.\n\r\n\rWhen the program begins, the game level is 0.\n\rWhen the game begins, the level is 1.\n\r\n\rThe LEDs on the board display the number of lives of the player.\n\rThe 7-segment display, displays the current game level.\n\rThe RGB LED is:\n\r\tWHITE - before gameplay begins (while in the menu)\n\r\tGREEN - while game is active\n\r\tBLUE  - when game is paused\n\r\tflashes RED - when air pump deployed\n\r\tPURPLE - when game is over\n\r\n\rSCORING:\n\r10 Points:  For each dirt that the player digs through\n\r50 Points:  For each slow enemy the player kills\n\r100 Points: For each fast enemy the player kills\n\r100 Points: When the player completes a level and goes to another.\n\r100 Points: For each life the player has remaining if the\n\r            game ends after 120 seconds.\n\r\n\rINSTRUCTIONS:\n\rPress 'w' (lowercase) to move player UP\n\rPress 'a' (lowercase) to move player LEFT\n\rPress 's' (lowercase) to move player DOWN\n\rPress 'd' (lowercase) to move player RIGHT\n\rPress 'j' (lowercase) to deploy the air pump\n\rPress 'q' (lowercase) to exit program (except while paused)\n\rPress push-button on board (P0.14) to pause game. Press again to unpause.\n\rPress 'z' (lowercase) when prompted to start new game\n\r\n\rPress 'Enter' to start game",0
	ALIGN
game_over_prompt = "\n\r\tGAME OVER\n\r\n\rPress 'z' to play again or press 'q' to quit.",0
	ALIGN
screen_padding = "   ",0
	ALIGN
quit_prompt = "\n\rYou have exited the program.\n\r",0
	ALIGN
game_over_code_executed = "\0",0
	ALIGN
q_PRESSED = "\0",0     ; 1 means user has pressed 'q' to quit program, 0 means they have not. Initialized to 0.
	ALIGN
ENTER_PRESSED = "\0",0 ; 1 means user has pressed 'ENTER' to begin game, 0 means they have not. Initialized t0 0.
	ALIGN
PAUSED = "\0",0		   ; 1 means game is paused, 0 means game is active; initialized to 0
	ALIGN
paused_string = "\tPAUSED",0
	ALIGN
SCORE = "\0\0\0\0",0	; Number of walls character has hit, initialized to 0
	ALIGN
SCORE_STRING = "\0\0\0\0\0\0\0\0",0		; ASCII String corresponding to SCORE variable
	ALIGN
player_position = "\0\0\0\0",0 ; current position of character. Equal to byte position in "string array" gb_0 thru gb_16. 
	ALIGN
player_position_j_press = "\0\0\0\0",0	; player position upon user 'j' press. Used for retract pump.
	ALIGN
player_direction = "\0\0\0\0",0 ; current direction player is facing (UP == -22, DOWN == 22, LEFT == -1, RIGHT == 1)
	ALIGN
player_direction_j_press = "\0\0\0\0",0	; player direction upon user 'j' press. Used for retract pump.
	ALIGN
enemy0_position = "\0\0\0\0",0	; position of fast enemy
	ALIGN
enemy0_position_init = "\0\0\0\0",0	; initial position of fast enemy
	ALIGN		
enemy1_position = "\0\0\0\0",0	; position of slow enemy 1
	ALIGN
enemy1_position_init = "\0\0\0\0",0	; initial position of slow enemy 1
	ALIGN		
enemy2_position = "\0\0\0\0",0	; position of slow enemy 2
	ALIGN
enemy2_position_init = "\0\0\0\0",0	; initial position of slow enemy 2
	ALIGN
enemy0_direction = "\0\0\0\0",0	; direction of fast enemy
	ALIGN
enemy1_direction = "\0\0\0\0",0	; direction of slow enemy 1
	ALIGN
enemy2_direction = "\0\0\0\0",0	; direction of slow enemy 2
	ALIGN
enemy0_alive = "\0",0		; flag: 1 alive, 0 dead
	ALIGN
enemy1_alive = "\0",0		; flag: 1 alive, 0 dead
	ALIGN
enemy2_alive = "\0",0		; flag: 1 alive, 0 dead
	ALIGN
air_pump_position = "\0\0\0\0",0 ; position of air pump nossle 
	ALIGN
retract_pump = "\0",0		; flag: 1 means retract pump, 0 means not to
	ALIGN
enemies_remaining = "\0",0  ; Number of enemies still alive
	ALIGN			
FORM_FEED = "\f",0
	ALIGN
w_PRESSED = "\0",0		; UP
	ALIGN
a_PRESSED = "\0",0		; LEFT
	ALIGN
s_PRESSED = "\0",0		; DOWN
	ALIGN
d_PRESSED = "\0",0		; RIGHT
	ALIGN
j_PRESSED = "\0",0		; Flag: 1 air pump deployed, 0 air pump not deployed
	ALIGN
user_input_already_taken = "\0",0
	ALIGN
game_over = "\0",0		; flag: 1 game is over, 0 game is active
	ALIGN
player_lives_remaining = "\0\0\0\0",0
	ALIGN
player_alive = "\0",0
	ALIGN
rand_32_bit = "\0\0\0\0",0		; Updated whenever user presses key on keyboard 3
	ALIGN
offset_1_bit = "\0\0\0\0",0		; Used for rand[0,1]
	ALIGN
offset_2_bit = "\0\0\0\0",0		; Used for rand[0,2], rand[0,3]
	ALIGN
offset_4_bit = "\0\0\0\0",0		; Used for rand[3,15]	(row position y val)
	ALIGN
offset_5_bit = "\0\0\0\0",0		; Used for rand[1,19]	(col position x val)
	ALIGN
slow_enemy_tick_flag = "\0",0	; If flag == 0, do NOT update slow enemy positions; if flag == 1, DO update
	ALIGN
		
; enemy_move_option_X is the enemy's next position
; We generate a random number to decide which of the 4 to use.
enemy_move_option_0 = "\0\0\0\0",0	 
	ALIGN
enemy_move_option_1 = "\0\0\0\0",0
	ALIGN
enemy_move_option_2 = "\0\0\0\0",0
	ALIGN
enemy_move_option_3 = "\0\0\0\0",0
	ALIGN
enemy_move_options_counter = "\0\0\0\0",0
	ALIGN
level_up = "\0",0
	ALIGN
current_level = "\0\0\0\0",0
	ALIGN
		
	
; Timer Counter (TC) Register holds the count
T0TC EQU 0xE0004008		   ; Timer0 Address
T1TC EQU 0xE0008008		   ; Timer1 Address
	
T0MCR EQU 0xE0004014	   ; Timer0 Match Control Register
T1MCR EQU 0xE0008014       ; Timer1 Match Control Register
	
MR1 EQU 0xE000401C		   ; Match Register 1 (Determines timeout period of Timer0 -- when interrupt is triggered. T0TC is reset upon interrupt.)
T1MR2 EQU 0xE0008020	   ; Mathc Register 2 for Timer 1
; Interrupt Registers
; Detecting Pending Interrupt: examine to determine if interrupt pending; Bit 1 is set if an interrupt is pending du to Match Register 1 (MR1) matching the Timer Count Register (TC)
; Clearing the Interrupt: If interrupt was due to MR1 matching TC, write a 1 to bit 1 in IR to clear interrupt.
T0IR EQU 0xE0004000		   ; Timer 0 Interrupt Register
T1IR EQU 0xE0008000		   ; Timer 1 Interrupt Register

; Timer Control Registers (TCR)
; Used to Enable / Disable Timer(s) and to Reset Timer Count Register (TC) to 0
; Bit 0: Set to 1 to Enable; Set to 0 to Disable
; Bit 1: Set to 1 to Reset Timer Count Register (TC) to 0
T0TCR EQU 0xE0004004
T1TCR EQU 0xE0008004
	
TIMEOUT_PERIOD = "\0\0\0\0",0		; Number of "ticks" after which Timer0 will trigger interrupt.
									; Initially equals 0x008CA000 (9216000), the number of ticks in 0.5 seconds at 18.432 MHz (14.7456 * (5/4) MHz)
	ALIGN
	
TIMEOUT_PERIOD_INIT EQU 0x008CA000
	ALIGN

; Strings corresponding to lab7 gameboard
; these strings are written to during the game (they change)
gb_0 =  "ZZZZZZZZZZZZZZZZZZZZZ\0"
gb_1 =  "Z                   Z\0"
gb_2 =  "Z                   Z\0"
gb_3 =  "Z###################Z\0"
gb_4 =  "Z###################Z\0"
gb_5 =  "Z###################Z\0"
gb_6 =  "Z###################Z\0"
gb_7 =  "Z###################Z\0"
gb_8 =  "Z###################Z\0"
gb_9 =  "Z###################Z\0"
gb_10 = "Z###################Z\0"
gb_11 = "Z###################Z\0"
gb_12 = "Z###################Z\0"
gb_13 = "Z###################Z\0"
gb_14 = "Z###################Z\0"
gb_15 = "Z###################Z\0"
gb_16 = "ZZZZZZZZZZZZZZZZZZZZZ\0"

	ALIGN

; These are gameboard setup strings
; Any time the game starts or resets, the
; gameboard strings above are initialized to these values.
gb_0_init =  "ZZZZZZZZZZZZZZZZZZZZZ\0"
gb_1_init =  "Z                   Z\0"
gb_2_init =  "Z                   Z\0"
gb_3_init =  "Z###################Z\0"
gb_4_init =  "Z###################Z\0"
gb_5_init =  "Z###################Z\0"
gb_6_init =  "Z###################Z\0"
gb_7_init =  "Z###################Z\0"
gb_8_init =  "Z###################Z\0"
gb_9_init =  "Z###################Z\0"
gb_10_init = "Z###################Z\0"
gb_11_init = "Z###################Z\0"
gb_12_init = "Z###################Z\0"
gb_13_init = "Z###################Z\0"
gb_14_init = "Z###################Z\0"
gb_15_init = "Z###################Z\0"
gb_16_init = "ZZZZZZZZZZZZZZZZZZZZZ\0"

	ALIGN
	
	
lab7			STMFD sp!, {lr}
				
				; Call our setup functions
				BL pin_connect_block_setup_for_uart0
				BL uart_init
				BL interrupt_init
				
				; Enable Timer1 use for random number generation 
				LDR r4, =T1TCR
				LDR r1, [r4]
				ORR r1, r1, #0x01
				STR r1, [r4]
				
				BL before_gameplay_begins
				
loop_q			LDR r0, =q_PRESSED	; Load q_PRESSED address
				LDRB r1, [r0]		; Load q_PRESSED memory contents (single byte)
				CMP r1, #1          ; Compare contents to 1
				BEQ	quit2            ; If contents == 1, goto quit
				
				B loop_q              ; Else loop again and check if 'q' was pressed
				
				
quit2			BL disable_interrupts	; If user has QUIT the program, we need to disable all further interrupts
				
				; Display exit message
				LDR r4, =quit_prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
end_quit_loop	ADD r0, r0, r0
				B end_quit_loop

				; Return to caller
				LDMFD sp!,{lr}
				BX lr
				

; Initialization code
; Resets flags pertaining to state before gameplay begins
; Prints menu, sets level and score back to 0, gives player 4 lives, sets RGB LED to white
before_gameplay_begins
				STMFD SP!, {r0-r12, lr}
				
				; Reset game_over to 0
				LDR r4, =game_over
				MOV r0, #0
				STRB r0, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #0
				STRB r0, [r4]
				
				LDR r4, =retract_pump
				MOV r0, #0
				STRB r0, [r4]
				
				LDR r4, =ENTER_PRESSED
				MOV r0, #0
				STRB r0, [r4]
				
				LDR r4, =game_over_code_executed
				MOV r0, #0
				STRB r0, [r4]
				
				; set level to 0
				LDR r4, =current_level
				MOV r0, #0
				STR r0, [r4]
				
				LDR r4, =player_lives_remaining
				MOV r0, #4
				STR r0, [r4]
				
				LDR r4, =player_alive
				MOV r0, #1
				STRB r0, [r4]
				
				; Display prompt / welcome message
				LDR r4, =prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Display user instructions
				LDR r4, =instructions
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				BL enter
				BL enter
				
				; set level to 1
				LDR r4, =current_level
				MOV r0, #0
				STR r0, [r4]
				
				; r0 contains new level
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore 
				
				; Set RGB_LED to white
				MOV r0, #0x5
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
		
				LDMFD SP!, {r0-r12, lr}
				BX lr             	 

; This subroutine increments level by 1, gives player 100 points for switching levels,
; Resets gameboard with new random enemy patterns, resets player to middle of dirt,
; sets all enemyX_alive flags back to alive (1), and decrements timeout period for
; Timer0 interrupt by 0.1 seconds down to min. of 0.1 sec.
level_up_code
				STMFD SP!, {r0-r12, lr}   ; Save registers
				
				
				; Set level_up flag back to 0
				LDR r4, =level_up
				MOV r0, #0
				STRB r0, [r4]
				
				; Increment level
				LDR r4, =current_level
				LDR r0, [r4]
				ADD r0, r0, #1
				STR r0, [r4]
				
				; Increment 7-seg to display level
				; r0 contains new level
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore 
				
				; Give player 100 points for leveling up!
				LDR r4, =SCORE
				LDR r0, [r4]
				ADD r0, r0, #100
				STR r0, [r4]
				
				
				BL reset_gameboard_strings
				; Get value of T1TC to generate random number
				LDR r4, =T1TC
				LDR r9, [r4]
				
				LDR r4, =rand_32_bit
				STR r9, [r4]
				
				BL enemy_init
				; Initialize player position to center of gameboard (position 186)
				LDR r4, =player_position
				MOV r0, #208 ;
				STR r0, [r4]
				
				LDR r4, =gb_0
				MOV r1, #0x76 		; 'v' == 0x76 (initialize player to facing-south position)
				STRB r1, [r4, r0]	; Store 0x76 at gb_0 address + offset to center of gameboard (208)

				LDR r4, =player_direction	; Set player direction DOWN
				MOV r1, #22
				STR r1, [r4]
				
				; Set all enemies to alive
				LDR r4, =enemy0_alive
				MOV r0, #1
				STRB r0, [r4]
				
				LDR r4, =enemy1_alive
				MOV r0, #1
				STRB r0, [r4]
				
				LDR r4, =enemy2_alive
				MOV r0, #1
				STRB r0, [r4]
				
				; Check current value of MR1
				; If it equals 1,843,200 we don't want to decrement it by 0.1 sec (1,843,200) again
				; "Build" Number: 1,843,200 == Number of "ticks" of the clock in 0.1 seconds
				MOV r2, #0x1C
				MOV r2, r2, LSL #4
				ADD r2, r2, #0x2
				MOV r2, r2, LSL #12
				
				LDR r0, =TIMEOUT_PERIOD
				LDR r1, [r0]
				
				CMP r2, r1
				BEQ do_not_decrement_timeout_period
				
				; Decrement TIMEOUT_PERIOD by 1,843,000
				SUB r1, r1, r2
				
				; Store TIMEOUT_PERIOD back to memory
				STR r1, [r0]
				
				; Modify MR1 value by setting it equal to updated TIMEOUT_PERIOD
				LDR r4, =MR1
				STR r1, [r4]		
				
do_not_decrement_timeout_period

				;BL print_gameboard
				
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
						
	
	LTORG
FIQ_Handler		STMFD SP!, {r0-r12, lr}   ; Save registers

TIMER1			; Check for a Timer1 interrupt due to MR2 matching TC
				LDR r4, =T1IR
				LDR r1, [r4]
				TST r1, #4		; If Bit 2 of T1IR is 1, there is an interrupt
				
				BEQ PUSH_BUTTON ; no timer1 interrupt, check for push_button interrupt
				
				; here, we know there is a timer1 interrupt, so service it
				
				; 120 seconds have elapsed so game is now over
				MOV r1, #1
				LDR r4, =game_over
				STRB r1, [r4]
				
				; clear timer1 interrupt
				LDR r4, =T1IR
				LDR r1, [r4]
				ORR r1, r1, #4
				STR r1, [r4]

				B FIQ_Exit2
				
				
PUSH_BUTTON		; Check for EINT1 interrupt
				; External Interrupt Flag Register (EXTINT) address
				; EINT0 - EINT3, Pins 0-3
				; 1 == Interrupt Pending
				LDR r0, =0xE01FC140    
				LDR r1, [r0]
				TST r1, #2      ; Note that TST takes the logical AND of r1 and #2, and sets CPSR upper four bits based on result
				BEQ UART02       ; If we have no EINT1 interrupt, goto UART0 to see if the interrupt came from UART0
			
				; here we have a push_button interrupt, so service it
				STMFD SP!, {r0-r12, lr}   ; Save registers, EINT1
				
				; If enter has not yet been pressed (we are still at the menu), we don't allow the player to pause the game
				LDR r4, =ENTER_PRESSED
				LDRB r0, [r4]
				CMP r0, #0
				BEQ PUSH_BUTTON_END
					
				; Check if game is currently paused
				LDR r4, =PAUSED
				LDRB r0, [r4]
				CMP r0, #1
				BEQ game_is_paused	; if paused (r0 == 1), goto game_is_paused
				
				; game_is_not_paused
				
				; Set PAUSED to 1
				MOV r1, #1
				STRB r1, [r4]
				
				; Set RGB_LED to blue
				MOV r0, #0x2
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				BL print_gameboard
				
				; Diable Timer0
				; 0xFFFFF014 is the interrupt disable register address
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x14] 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x14]
				
				; Pause Timer1
				LDR r4, =T1TCR
				LDR r9, [r4]
				BIC r9, r9, #0x01
				STR r9, [r4]
				
				B PUSH_BUTTON_END
							
game_is_paused

				; Set PAUSED to 1
				LDR r4, =PAUSED
				MOV r1, #0
				STRB r1, [r4]
				
				; Enable Timer0
				; 1 == enable, 0 == NO EFFECT
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x10] 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x10]
				
				; Unpause Timer1
				LDR r4, =T1TCR
				LDR r9, [r4]
				ORR r9, r9, #0x01
				STR r9, [r4]
				
				; Set RGB_LED to green
				MOV r0, #0x1
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				B PUSH_BUTTON_END
				
			
PUSH_BUTTON_END	LDMFD SP!, {r0-r12, lr}   ; Restore registers, EINT1
				
				; Used to clear the EINT1 (push button) interrupt
				ORR r1, r1, #2		; Clear Interrupt
				STR r1, [r0]		; r0 == External Interrupt Flag Register
									; Write a 1 to clear interrupt
									
				B FIQ_Exit2

UART02			; Check for UART0 interrupt
				; 0 in bit 0 of U0IIR == Interrupt Pending, 1 == no interrupt
				LDR r0, =0xE000C008			; r0 == UART Interrupt Identification Register (U0IIR)
				LDR r1, [r0]
				TST r1, #1					; If 1 in bit 0, z == 0 there is no interrupt in UART0
				BNE TIMER02	

				STMFD SP!, {r0-r12, lr}   ; Save registers, UART0
				
				; UART0 interrupt servicing code here
				
				; Get value of Timer 1 TC count
				LDR r4, =T1TC
				LDR r9, [r4]
				
				; Set Timer 1 TC value as new random 32 bit, from which random #'s are drawn
				LDR r4, =rand_32_bit
				STR r9, [r4]
				
				; Reset all random number offsets
				MOV r9, #0
				LDR r4, =offset_1_bit
				STR r9, [r4]
				
				LDR r4, =offset_2_bit
				STR r9, [r4]
				
				LDR r4, =offset_4_bit
				STR r9, [r4]
				
				LDR r4, =offset_5_bit
				STR r9, [r4]
				
				; Get character entered by user from UART0
				STMFD SP!, {r1-r12, lr}   ; Spill registers
				BL read_character
				LDMFD SP!, {r1-r12, lr}   ; Restore registers
				
				; check if game is over
				LDR r4, =game_over
				LDRB r1, [r4]
				CMP r1, #1
				BNE not_game_over_uart
				
				; check if user entered 'q'
				CMP r0, #0x71		
				BEQ q_label2
				
				; check for 'z' to continue
				CMP r0, #0x7A
				BLEQ before_gameplay_begins
				
				B UART0_END2
				
				
not_game_over_uart
				
				LDR r4, =user_input_already_taken
				LDRB r2, [r4]
				CMP r2, #1
				BEQ UART0_END2
				
				; Check if enter has already been pressed; if not, ignore other inputs except 'q' and 'enter'
				LDR r4, =ENTER_PRESSED
				LDRB r1, [r4]
				CMP r1, #1
				BEQ check_more
	
				; Check if entered ASCII character is 'ENTER' (carriage return)
				CMP r0, #0x0D	
				BEQ enter_label2
				
				; check if user entered 'q'
				CMP r0, #0x71
				BEQ q_label2
				
				B UART0_END2
						
	LTORG

; user pressed 'enter', this handles that
enter_label2
				
				; Set ENTER_PRESSED flag == 1
				LDR r4, =ENTER_PRESSED
				MOV r1, #0x1
				STRB r1, [r4]
				
				BL reset_gameboard_strings
				; Get calue of T1TC to generate random number
				LDR r4, =T1TC
				LDR r9, [r4]
				
				; set T1TC contents as new random 32 bit number
				LDR r4, =rand_32_bit
				STR r9, [r4]
				
				; set score to 0
				LDR r4, =SCORE
				MOV r0, #0
				STR r0, [r4]
				
				LDR r4, =retract_pump
				MOV r10, #0
				STRB r10, [r4]
				
				; randomize enemy positions and empty tunnels around them
				BL enemy_init
				
				; Initialize player position to center of gameboard (position 208)
				LDR r4, =player_position
				MOV r0, #208 ;
				STR r0, [r4]
				
				LDR r4, =gb_0
				MOV r1, #0x76 		; 'v' == 0x76 (initialize player to facing-south position)
				STRB r1, [r4, r0]	; Store 0x76 at gb_0 address + offset to center of gameboard (208)

				LDR r4, =player_direction	; Set player direction DOWN
				MOV r1, #22
				STR r1, [r4]
				
				; Set all enemies to alive
				LDR r4, =enemy0_alive
				MOV r0, #1
				STRB r0, [r4]
				
				LDR r4, =enemy1_alive
				MOV r0, #1
				STRB r0, [r4]
				
				LDR r4, =enemy2_alive
				MOV r0, #1
				STRB r0, [r4]
				
				; give player 4 lives
				LDR r4, =player_lives_remaining
				MOV r0, #4
				STR r0, [r4]
				
				; set level to 1
				LDR r4, =current_level
				MOV r0, #1
				STR r0, [r4]
				
				; r0 contains new level
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore 
				
				; Changes LEDs based on number of lives left
				MOV r0, #0xF
				STMFD SP!, {r0-r12, lr}
				BL illuminateLEDs
				LDMFD SP!, {r0-r12, lr}
				
				; Set RGB_LED to green
				MOV r0, #0x1
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				
				BL print_gameboard
				
				
				; Enable / Start Timer0
				LDR r4, =MR1
				LDR r0, =TIMEOUT_PERIOD
				LDR r5, =TIMEOUT_PERIOD_INIT
				STR r5, [r0]
				STR r5, [r4]			; Initialize MR1 value to 9,216,000 ticks == 1 update per half-second
				
				LDR r4, =T0TCR
				LDR r0, [r4]
				ORR r0, r0, #1			; Bit_0 == 1 <=> Enable
				STR r0, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]
				
				;Timer 1 interrupt enable
				LDR r4, =0xFFFFF000
				LDR r9, [r4,#0x10]
				ORR r9, r9, #0x20
				STR r9, [r4,#0x10]
				
				; Enable Interrupts
				; 1 == enable, 0 == NO EFFECT
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x10] 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x10]
				
				; Grab current Timer1 count
				LDR r4, =T1TC
				LDR r10, [r4]
				
				
				; Add 2 minutes to current Timer1 count and set as T1MR2 value
				LDR r4, =T1MR2
				MOV r9, #0x83
				MOV r9, r9, LSL #8
				ADD r9, r9, #0xD6
				MOV r9, r9, LSL #16
				ADD r9, r9, r10
				STR r9, [r4]
				
				;Enable Timer1 to Interrupt
				LDR r4, =T1MCR
				LDR r9, [r4]
				ORR r9, r9, #0xC0
				STR r9, [r4]
				
				B UART0_END2
				
	LTORG
				
check_more
				; if we are paused, we don't accept any UART0 input
				LDR r4, =PAUSED
				LDRB r9, [r4]
				CMP r9, #1
				BEQ UART0_END2
				
				; Check if entered ASCII character is 'q'
				CMP r0, #0x71		
				BEQ q_label2

				CMP r0, #0x77		; Check if entered ASCII character is 'w'
				BEQ w_label2
				
				CMP r0, #0x61		; Check if entered ASCII character is 'a'
				BEQ a_label2
				
				CMP r0, #0x73		; Check if entered ASCII character is 's'
				BEQ s_label2
				
				CMP r0, #0x64		; Check if entered ASCII character is 'd'
				BEQ d_label2
				
				CMP r0, #0x6A		; Check if entered ASCII character is 'j'
				BEQ j_label2
				
				B UART0_END2
				
q_label2		LDR r4, =q_PRESSED		
				MOV r1, #1
				STRB r1, [r4]			; Set q_PRESSED to 1
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]
				
				B UART0_END2
					

w_label2		LDR r4, =w_PRESSED		
				MOV r1, #1
				STRB r1, [r4]

				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]

				B UART0_END2
				
a_label2		LDR r4, =a_PRESSED		
				MOV r1, #1
				STRB r1, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]

				B UART0_END2

s_label2		LDR r4, =s_PRESSED		
				MOV r1, #1
				STRB r1, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]

				B UART0_END2

d_label2		LDR r4, =d_PRESSED		
				MOV r1, #1
				STRB r1, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]

				B UART0_END2
				
j_label2		LDR r4, =j_PRESSED		
				MOV r1, #1
				STRB r1, [r4]
				
				LDR r4, =user_input_already_taken
				MOV r0, #1
				STRB r0, [r4]
				
				; Save player_position at 'j' press in player_position_j_press
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =player_position_j_press
				STR r0, [r4]
				
				; Save player_direction at 'j' press in player_direction_j_press
				LDR r4, =player_direction
				LDR r0, [r4]
				LDR r4, =player_direction_j_press
				STR r0, [r4]

				B UART0_END2
				
				
UART0_END2		LDMFD SP!, {r0-r12, lr}     ; Restore registers, UART0
				B FIQ_Exit2
				

TIMER02			; Check for a Timer0 interrupt due to MR1 matching TC
				LDR r4, =T0IR
				LDR r1, [r4]
				TST r1, #2		; If Bit 1 of T0IR is 1, there is an interrupt
				
				LDR r4, =user_input_already_taken
				MOV r0, #0
				STRB r0, [r4]
				
				BEQ FIQ_Exit2	; If Bit 1 of TOIR is 0, there is NO interrupt, so goto FIQ_Exit (in this case, z==0 in CPSR)
				
				
				STMFD SP!, {r0-r12, lr}   ; Save registers, TIMER0
				
				; Code to service TIMER0 interrupt
				

				; Check if game_over == 1
				LDR r4, =game_over
				LDRB r0, [r4]
				CMP r0, #1
				BNE game_not_over
				
				; game_over_code follows
				
game_over_code
				
				; check if game_over_code has already been executed (we only want to do it once per game over)
				LDR r4, =game_over_code_executed
				LDRB r0, [r4]
				CMP r0, #1
				BEQ game_over_end
				
				
				; Set RGB_LED to purple
				MOV r0, #0x3
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				LDR r4, =player_lives_remaining
				LDR r0, [r4]
				MOV r1, #0			; counter for loop

; give player an extra 100 points for every remaining life they had at game_over
lives_bonus_loop
				CMP r0, #0
				BEQ lives_bonus_loop_end
				ADD r1, r1, #100
				SUB r0, r0, #1
				B lives_bonus_loop
			
lives_bonus_loop_end

				; Update SCORE in memory
				LDR r4, =SCORE
				LDR r2, [r4]
				ADD r2, r2, r1
				STR r2, [r4]

				BL print_gameboard
				
				LDR r4, =game_over_prompt
				STMFD SP!, {r0-r12, lr}
				BL output_string
				LDMFD SP!, {r0-r12, lr}
				
				; 0xFFFFF014 is the interrupt disable register address
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x14] 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x14]
				
				LDR r4, =game_over_code_executed
				MOV r0, #1
				STRB r0, [r4]
				
				LDR r4, =ENTER_PRESSED
				MOV r0, #0
				STRB r0, [r4]
				
				B game_over_end
				
game_not_over				
				
				; Set RGB_LED to green
				MOV r0, #0x1
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				
check_pump				
				
				; Check retract_pump flag 
				LDR r4, =retract_pump
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BNE check_level_up
				
				LDR r4, =retract_pump
				MOV r1, #0
				STRB r1, [r4]
				
				; retract_pump == 1 here
				LDR r4, =player_direction_j_press
				LDR r1, [r4]
				LDR r4, =player_position_j_press
				LDR r0, [r4]
				LDR r4, =air_pump_position
				LDR r2, [r4]
				
				; Figure out how many spaces the air pump had previously been shot out to
				; div_and_mod is used to calculate: 
				; (air_pump_position - player_position_j_press) / player_direction_j_press
				; For example, >***#Z with player_position_j_press of 152
				; would result in calculation of (155 - 152) / (1) = 3
				; This would result in writing 3 spaces to the places in memory where the *** are
				; After the following code, the picture will look like
				; >   #Z
				SUB r0, r2, r0
				STMFD SP!, {r2-r12, lr}   ; Save registers
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}   
				
				LDR r4, =player_direction_j_press
				LDR r1, [r4]
				
				; Only execute following code if quotient from div_and_mod is in range
				; [1,3] inclusive
				CMP r0, #0
				BLT check_j
				
				CMP r0, #3
				BGT check_j
				
				; Quotient from div_and_mod in r0 is our loop counter
retract_loop	MOV r4, #0
				CMP r4, r0
				BEQ check_j
				
				LDR r4, =gb_0
				MOV r3, #0x20				; 0x20 == ASCII 'Space'
				STRB r3, [r4, r2]			; Store 0x20 at byte pointed to by offset r2 (air_pump_position) in gb_0 string
				SUB r2, r2, r1				; air_pump_position := air_pump_position - player_direction_j_press
				SUB r0, r0, #1				; decrement loop count by 1
				B retract_loop
				
check_level_up				
				; Check if level_up == 1
				
				LDR r4, =level_up
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_j
				; Here we know level up is 1
				
				BL level_up_code
				
				B clear_timer0_interrupt
				
				
				
check_j			; Check if j_PRESSED == 1
				LDR r4, =j_PRESSED
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BEQ j_PRESSED_service

check_w			; Check if w_PRESSED == 1
				LDR r4, =w_PRESSED
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BEQ w_PRESSED_service
				
				; Check if a_PRESSED == 1
				LDR r4, =a_PRESSED
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BEQ a_PRESSED_service
				
				; Check if s_PRESSED == 1
				LDR r4, =s_PRESSED
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BEQ s_PRESSED_service
				
				; Check if d_PRESSED == 1
				LDR r4, =d_PRESSED
				LDRB r0, [r4]
				MOV r1, #1
				CMP r0, r1
				BEQ d_PRESSED_service
				
				B update_enemy_positions

; j_PRESSED_service sets RGB_LEC to red as the air pump has been deployed
j_PRESSED_service

				; set RGB_LED to RED
				MOV r0, #0x0
				STMFD SP!, {r0-r12, lr}
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r12, lr}
				
				; Initialize air_pump_position to player_position
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =air_pump_position
				STR r0, [r4]				; r0 == player_position == air_pump_position

				LDR r4, =player_direction
				LDR r1, [r4]				; r1 == player_direction
				
				MOV r6, #3					; loop counter for 3 iterations

j_PRESSED_service_loop
				
				; Check if loop counter has reached 0. If so, exit loop.
				MOV r8, #0
				CMP r6, r8
				BEQ exit_j_PRESSED_service_loop		
				
				ADD r2, r0, r1				; r2 := air_pump_position + player_direction
				LDR r4, =gb_0				; Base address of gameboard strings
				LDRB r3, [r4, r2]			; Load byte at offset r2
				MOV r5, #0x23				; 0x23 == ASCII for '#' (DIRT)
				CMP r3, r5					; Check if byte in r3 is '#' (DIRT)
				BEQ exit_j_PRESSED_service_loop
				; here we know r3 does not equal '#'
				MOV r5, #0x5A				; 0x5A == ASCII for 'Z' (WALL)
				CMP r3, r5					; Check if byte in r3 is 'Z' (WALL)
				BEQ exit_j_PRESSED_service_loop
				; here we know r3 does not equal 'Z' (and also not '#')
				
				; Check if air_pump_position equals any enemy position
				
				; Check if air_pump_position == enemy0_position
				LDR r4, =enemy0_position
				LDR r9, [r4]
				CMP r2, r9
				BNE check_enemy1_pos
				
				LDR r4, =enemy0_alive
				LDRB r9, [r4]
				CMP r9, #0
				BEQ check_enemy1_pos
				
				; we hit enemy0, so kill him
				MOV r9, #0
				STRB r9, [r4]
				
				LDR r4, =SCORE
				LDR r9, [r4]
				ADD r9, r9, #100			; Fast enemy 'B', so SCORE := 100
				STR r9, [r4]
				
check_enemy1_pos

				; Check if air_pump_position == enemy1_position
				LDR r4, =enemy1_position
				LDR r9, [r4]
				CMP r2, r9
				BNE check_enemy2_pos
				
				LDR r4, =enemy1_alive
				LDRB r9, [r4]
				CMP r9, #0
				BEQ check_enemy2_pos
				
				; we hit enemy1, so kill him
				MOV r9, #0
				STRB r9, [r4]
				
				LDR r4, =SCORE
				LDR r9, [r4]
				ADD r9, r9, #50			; Slow enemy 'b', so SCORE := 50
				STR r9, [r4]
				
check_enemy2_pos

				; Check if air_pump_position == enemy2_position
				LDR r4, =enemy2_position
				LDR r9, [r4]
				CMP r2, r9
				BNE update_pump_pos
				
				LDR r4, =enemy2_alive
				LDRB r9, [r4]
				CMP r9, #0
				BEQ update_pump_pos
				
				; we hit enemy2, so kill him
				MOV r9, #0
				STRB r9, [r4]
				
				LDR r4, =SCORE
				LDR r9, [r4]
				ADD r9, r9, #50			; Slow enemy 'b', so SCORE := 50
				STR r9, [r4]

update_pump_pos
				
				; air_pump_position := air_pump_position + player_direction
				MOV r0, r2
				LDR r9, =air_pump_position
				STR r0, [r9]
				
				LDR r4, =gb_0
				MOV r7, #0x2A				; 0x2A == ASCII for '*'
				STRB r7, [r4, r0]			; Store byte 0x2A at offset air_pump_position in gb_0
				
				SUB r6, r6, #1				; Decrement loop counter by 1
				
				B j_PRESSED_service_loop	; loop again
				
exit_j_PRESSED_service_loop

				LDR r4, =j_PRESSED		
				MOV r1, #0
				STRB r1, [r4]
				
				; now that we've deployed pump, set this flag so that on
				; the next timer0 interrupt, we retract the pump
				LDR r4, =retract_pump
				MOV r0, #1
				STRB r0, [r4]
				
				; update enemy positions by taking random directions
				B update_enemy_positions

; When user presses 'w', this handles that
; 'w' is going UP
w_PRESSED_service
				
				LDR r4, =player_position
				LDR r0, [r4]
				
				MOV r2, r0			; temp old player position
				SUB r0, r0, #22		; new player position in r0
				
				LDR r9, =gb_0
				LDRB r10, [r9, r0]
				CMP r10, #0x23 		; 0x23 == '#' DIRT
				BNE not_in_dirt_w
				
				; r10 is dirt, update score += 10
				LDR r9, =SCORE
				LDR r10, [r9]
				ADD r10, r10, #10
				STR r10, [r9]
				
not_in_dirt_w	MOV r3, #42			; if new position is less than or equal to 20
				CMP r0, r3			; then it is inside of the north border
				MOVLE r0, r2		; so set r0 back to old position
				
				STR r0, [r4]
				
				MOV r3, #0x20		; 'Space' == 0x20
				MOV r5, #0x5E		; '^' == 0x5E (set player facing north)
				
				LDR r4, =gb_0
				STRB r3, [r4, r2]	; Store 'Space' at old player position
				STRB r5, [r4, r0]	; Store '^' at new player position
				
				LDR r4, =player_direction	; Set player direction
				MOV r1, #-22
				STR r1, [r4]
				
				LDR r4, =w_PRESSED		
				MOV r1, #0
				STRB r1, [r4]
				
				B check_if_we_die_1

; When user presses 'a', this handles that
; 'a' is going LEFT
a_PRESSED_service
				
				LDR r4, =player_position
				LDR r5, [r4]
				MOV r2, r5			; temp old player position
				SUB r5, r5, #1		; new player position in r5
				
				MOV r0, r5			; set dividend for div_and_mod call
				MOV r1, #22			; set divisor for div_and_mod call
									; if new position is divisible by 22 then new position is inside west border
				
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				
				LDR r9, =gb_0
				LDRB r10, [r9, r5]
				CMP r10, #0x23 		; 0x23 == '#' DIRT
				BNE not_in_dirt_a
				
				; r10 is dirt, update score += 10
				LDR r9, =SCORE
				LDR r10, [r9]
				ADD r10, r10, #10
				STR r10, [r9]
				
not_in_dirt_a	CMP r1, #0			; compare remainder to 0
				MOVEQ r5, r2		; if r1 == 0, set r5 back to old position
				
				STR r5, [r4]
				
				MOV r6, #0x20		; 'Space' == 0x20
				MOV r7, #0x3C		; '<' == 0x3C (set player facing west)
				
				LDR r4, =gb_0
				STRB r6, [r4, r2]	; Store 'Space' at old player position
				STRB r7, [r4, r5]	; Store '<' at new player position
				
				LDR r4, =player_direction	; Set player direction
				MOV r1, #-1
				STR r1, [r4]
				
				LDR r4, =a_PRESSED		
				MOV r1, #0
				STRB r1, [r4]

				B check_if_we_die_1


; When user presses 's', this handles that
; 's' is going DOWN
s_PRESSED_service
				
				LDR r4, =player_position
				LDR r0, [r4]
				MOV r2, r0			; temp old player position
				ADD r0, r0, #22		; new player position in r0
				
				LDR r9, =gb_0
				LDRB r10, [r9, r0]
				CMP r10, #0x23 		; 0x23 == '#' DIRT
				BNE not_in_dirt_s
				
				; r10 is dirt, update score += 10
				LDR r9, =SCORE
				LDR r10, [r9]
				ADD r10, r10, #10
				STR r10, [r9]
				
not_in_dirt_s				
									; Build 0x161 == 352
				MOV r3, #0x16		; if new position is greater than or equal to 352
				MOV r3, r3, LSL #4
				CMP r0, r3			; then it is inside of the south border
				MOVGE r0, r2		; so set r0 back to old position
				
				STR r0, [r4]
				
				MOV r3, #0x20		; 'Space' == 0x20
				MOV r5, #0x76		; 'v' == 0x3C (set player facing south)
				
				LDR r4, =gb_0
				STRB r3, [r4, r2]	; Store 'Space' at old player position
				STRB r5, [r4, r0]	; Store 'v' at new player position
				
				LDR r4, =player_direction	; Set player direction
				MOV r1, #22
				STR r1, [r4]
				
				LDR r4, =s_PRESSED		
				MOV r1, #0
				STRB r1, [r4]

				B check_if_we_die_1

; When user presses 'd', this handles that
; 'd' is going RIGHT
d_PRESSED_service

				LDR r4, =player_position
				LDR r5, [r4]
				MOV r2, r5			; temp old player position
				ADD r5, r5, #1		; new player position in r5
				
				MOV r0, r5			; set dividend for div_and_mod call
				ADD r0, r0, #2		; add 2 to dividend
				MOV r1, #22			; if (new position + 2) % 22 == 0, then 
									; new position is inside east border
									
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				
				LDR r9, =gb_0
				LDRB r10, [r9, r5]
				CMP r10, #0x23 		; 0x23 == '#' DIRT
				BNE not_in_dirt_d
				
				; r10 is dirt, update score += 10
				LDR r9, =SCORE
				LDR r10, [r9]
				ADD r10, r10, #10
				STR r10, [r9]
				
not_in_dirt_d	CMP r1, #0			; compare remainder to 0
				MOVEQ r5, r2		; if r1==0 set r5 back to old position
				
				STR r5, [r4]
				
				MOV r6, #0x20		; 'Space' == 0x20
				MOV r7, #0x3E		; '>' == 0x3E (set player facing east)
				
				LDR r4, =gb_0
				STRB r6, [r4, r2]	; Store 'Space' at old player position
				STRB r7, [r4, r5]	; Store '>' at new player position
				
				LDR r4, =player_direction	; Set player direction
				MOV r1, #1
				STR r1, [r4]
				
				LDR r4, =d_PRESSED		
				MOV r1, #0
				STRB r1, [r4]
				

;;;;;;;;;;; Did any enemy kill us? ;;;;;;;;;;;;;;				
check_if_we_die_1
				
				; Check if enemy0 killed us
				; Only check if enemy0 is still alive
				LDR r4, =enemy0_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE player_killed_by_enemy1_0
				
				; Check if player_pos == enemy0_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy0_position
				LDR r1, [r4]
				CMP r0, r1
				BNE player_killed_by_enemy1_0
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				B reinitialize_game
				
player_killed_by_enemy1_0

				; Check if enemy1 killed us
				; Only check if enemy1 is still alive
				LDR r4, =enemy1_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE player_killed_by_enemy2_0
				
				; Check if player_pos == enemy1_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy1_position
				LDR r1, [r4]
				CMP r0, r1
				BNE player_killed_by_enemy2_0
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				B reinitialize_game
				
	LTORG
				
				
player_killed_by_enemy2_0

				; Check if enemy0 killed us
				; Only check if enemy2 is still alive
				LDR r4, =enemy2_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE update_enemy_positions

				; Check if player_pos == enemy0_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy2_position
				LDR r1, [r4]
				CMP r0, r1
				BNE update_enemy_positions
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
			
				B reinitialize_game
				
				
				
				
update_enemy_positions				
				
				; If enemy0 dead, don't update its position and skip over
				LDR r4, =enemy0_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE enemy0_dead
				
				; update enemy0_pos
				
				; reset enemy_move_options_counter to 0
				LDR r4, =enemy_move_options_counter
				MOV r0, #0
				STR r0, [r4]
query_top_0
				LDR r4, =enemy0_position
				LDR r0, [r4]
				MOV r1, r0			; Store enemy0_position in r1 as temp
				
				; Query top position
				SUB r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_left_0
				
				; if here, r2 is not dirt
				
				CMP r0, #42
				BLT query_left_0
				
				; if here, r0 is valid, so store it at enemy_move_option_0
				LDR r4, =enemy_move_option_0
				STR r0, [r4]
				
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r0, [r4]
				ADD r0, r0, #1
				STR r0, [r4]
				
query_left_0	
				MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query left position
				SUB r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_bottom_0
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_bottom_0
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
query_bottom_0	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_right_0
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_right_0
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
query_right_0	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ decide_where_to_move_enemy_0
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ decide_where_to_move_enemy_0
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
				CMP r3, #3
				LDREQ r4, =enemy_move_option_3
				STREQ r0, [r4]
				
decide_where_to_move_enemy_0
				
				LDR r4, =enemy_move_options_counter
				LDR r1, [r4]
				
				; Enemy has no place to move, skip over
				CMP r1, #0
				BEQ enemy0_dead
				
				CMP r1, #1
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]		; r2 is new enemy0 position
				LDREQ r4, =enemy0_position
				LDREQ r3, [r4]		; old enemy0 position
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]		; update enemy0 position to new position
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]	; store space at old position
				MOVEQ r3, #0x42		; 'B' fast enemy
				STRBEQ r3, [r4,r2]	; store 'B' at new position
				BEQ enemy0_dead
				
				SUB r1, r1, #1
				MOV r0, #0
				
				; Call rand_a_b with a == 0 and b == enemy_move_options_counter - 1
				; e.g., if enemy_move_options_counter == 2,
				; 		then getting rand[0,1] 
				BL rand_a_b
				
				; If r0 == 0, use enemy_move_option_0
				; If r0 == 1, use 				   _1
				; If r0 == 2, use 				   _2
				; If r0 == 3, use 				   _3
				CMP r0, #0
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]
				LDREQ r4, =enemy0_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x42		; 'B' fast enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #1
				LDREQ r4, =enemy_move_option_1
				LDREQ r2, [r4]
				LDREQ r4, =enemy0_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x42		; 'B' fast enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #2
				LDREQ r4, =enemy_move_option_2
				LDREQ r2, [r4]
				LDREQ r4, =enemy0_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x42		; 'B' fast enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #3
				LDREQ r4, =enemy_move_option_3
				LDREQ r2, [r4]
				LDREQ r4, =enemy0_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x42		; 'B' fast enemy
				STRBEQ r3, [r4,r2]
				
				

enemy0_dead		
				; reset enemy_move_options_counter to 0
				LDR r4, =enemy_move_options_counter
				MOV r0, #0
				STR r0, [r4]
				
				; If flag == 0, ski_slow_enemies -- we only update their positions every two timer0 interrupt "ticks"
				LDR r4, =slow_enemy_tick_flag
				LDRB r0, [r4]
				CMP r0, #1
				BNE skip_slow_enemies

				LDR r4, =enemy1_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE enemy1_dead
				
				; update enemy1_pos

query_top_1
				LDR r4, =enemy1_position
				LDR r0, [r4]
				MOV r1, r0			; Store enemy0_position in r1 as temp
				
				; Query top position
				SUB r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_left_1
				
				; if here, r2 is not dirt
				
				CMP r0, #42
				BLT query_left_1
				
				; if here, r0 is valid, so store it at enemy_move_option_0
				LDR r4, =enemy_move_option_0
				STR r0, [r4]
				
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r0, [r4]
				ADD r0, r0, #1
				STR r0, [r4]
				
query_left_1	
				MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query left position
				SUB r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_bottom_1
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_bottom_1
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
query_bottom_1	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_right_1
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_right_1
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
query_right_1	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ decide_where_to_move_enemy_1
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ decide_where_to_move_enemy_1
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
				CMP r3, #3
				LDREQ r4, =enemy_move_option_3
				STREQ r0, [r4]
				
decide_where_to_move_enemy_1
				
				LDR r4, =enemy_move_options_counter
				LDR r1, [r4]
				
				CMP r1, #0
				BEQ enemy1_dead
				
				CMP r1, #1
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]
				LDREQ r4, =enemy1_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				BEQ enemy1_dead
				
				SUB r1, r1, #1
				MOV r0, #0
				
				; Call rand_a_b with a == 0 and b == enemy_move_options_counter - 1
				; e.g., if enemy_move_options_counter == 2,
				; 		then getting rand[0,1] 
				BL rand_a_b
				
				; If r0 == 0, use enemy_move_option_0
				; If r0 == 1, use 				   _1
				; If r0 == 2, use 				   _2
				; If r0 == 3, use 				   _3
				CMP r0, #0
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]
				LDREQ r4, =enemy1_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #1
				LDREQ r4, =enemy_move_option_1
				LDREQ r2, [r4]
				LDREQ r4, =enemy1_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #2
				LDREQ r4, =enemy_move_option_2
				LDREQ r2, [r4]
				LDREQ r4, =enemy1_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #3
				LDREQ r4, =enemy_move_option_3
				LDREQ r2, [r4]
				LDREQ r4, =enemy1_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
enemy1_dead		
				; reset enemy_move_options_counter to 0
				LDR r4, =enemy_move_options_counter
				MOV r0, #0
				STR r0, [r4]
				
				LDR r4, =enemy2_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE enemy2_dead
				
				; update enemy2_pos
				
query_top_2
				LDR r4, =enemy2_position
				LDR r0, [r4]
				MOV r1, r0			; Store enemy0_position in r1 as temp
				
				; Query top position
				SUB r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_left_2
				
				; if here, r2 is not dirt
				
				CMP r0, #42
				BLT query_left_2
				
				; if here, r0 is valid, so store it at enemy_move_option_0
				LDR r4, =enemy_move_option_0
				STR r0, [r4]
				
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r0, [r4]
				ADD r0, r0, #1
				STR r0, [r4]
				
query_left_2	
				MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query left position
				SUB r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_bottom_2
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_bottom_2
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
query_bottom_2	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #22
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ query_right_2
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ query_right_2
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
query_right_2	MOV r0, r1			; reset r0 to original enemy0_position
				
				;Query bottom position
				ADD r0, r0, #1
				LDR r4, =gb_0
				LDRB r2, [r4,r0]
				CMP r2, #0x23		; '#' == 0x23
				BEQ decide_where_to_move_enemy_2
				
				; if here, r2 is not dirt
				
				CMP r2, #0x5A		; 'Z' == 0x5A
				BEQ decide_where_to_move_enemy_2
				
				; if here, r2 is not in the wall
			
				; increment enemy_move_options_counter by 1
				LDR r4, = enemy_move_options_counter
				LDR r2, [r4]
				MOV r3, r2			; Old enemy_move_options_counter
				ADD r2, r2, #1		; Increment enemy_move_options_counter by 1
				STR r2, [r4]
				
				; Compare old enemy_move_options_counter to see where in memory we store the valid new position
				CMP r3, #0
				LDREQ r4, =enemy_move_option_0
				STREQ r0, [r4]
				
				CMP r3, #1
				LDREQ r4, =enemy_move_option_1
				STREQ r0, [r4]
				
				CMP r3, #2
				LDREQ r4, =enemy_move_option_2
				STREQ r0, [r4]
				
				CMP r3, #3
				LDREQ r4, =enemy_move_option_3
				STREQ r0, [r4]
				
decide_where_to_move_enemy_2
				
				LDR r4, =enemy_move_options_counter
				LDR r1, [r4]
				
				CMP r1, #0
				BEQ enemy2_dead
				
				CMP r1, #1
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]
				LDREQ r4, =enemy2_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				BEQ enemy2_dead
				
				SUB r1, r1, #1
				MOV r0, #0
				
				; Call rand_a_b with a == 0 and b == enemy_move_options_counter - 1
				; e.g., if enemy_move_options_counter == 2,
				; 		then getting rand[0,1] 
				BL rand_a_b
				
				; If r0 == 0, use enemy_move_option_0
				; If r0 == 1, use 				   _1
				; If r0 == 2, use 				   _2
				; If r0 == 3, use 				   _3
				CMP r0, #0
				LDREQ r4, =enemy_move_option_0
				LDREQ r2, [r4]
				LDREQ r4, =enemy2_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #1
				LDREQ r4, =enemy_move_option_1
				LDREQ r2, [r4]
				LDREQ r4, =enemy2_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #2
				LDREQ r4, =enemy_move_option_2
				LDREQ r2, [r4]
				LDREQ r4, =enemy2_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
				CMP r0, #3
				LDREQ r4, =enemy_move_option_3
				LDREQ r2, [r4]
				LDREQ r4, =enemy2_position
				LDREQ r3, [r4]
				MOVEQ r5, #0x20		; 'Space'
				STREQ r2, [r4]
				LDREQ r4, =gb_0
				STRBEQ r5, [r4,r3]
				MOVEQ r3, #0x62		; 'b' slow enemy
				STRBEQ r3, [r4,r2]
				
enemy2_dead
				; reset enemy_move_options_counter to 0
				LDR r4, =enemy_move_options_counter
				MOV r0, #0
				STR r0, [r4]
				
skip_slow_enemies
				
				; Re-assert all enemies who are alive
				
				LDR r4, =enemy0_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_enemy1_alive
				
				; Enemy 0 alive
				LDR r4, =enemy0_position
				LDR r0, [r4]
				LDR r4, =gb_0
				MOV r2, #0x42
				STRB r2, [r4,r0]
				
check_enemy1_alive
				LDR r4, =enemy1_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_enemy2_alive
				
				; Enemy 1 alive
				LDR r4, =enemy1_position
				LDR r0, [r4]
				LDR r4, =gb_0
				MOV r2, #0x62
				STRB r2, [r4,r0]

check_enemy2_alive
				LDR r4, =enemy2_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_if_enemies_kill_us
				
				; Enemy 2 alive
				LDR r4, =enemy2_position
				LDR r0, [r4]
				LDR r4, =gb_0
				MOV r2, #0x62
				STRB r2, [r4,r0]

;;;;;;;;;;; Did any enemy kill us? ;;;;;;;;;;;;;;
check_if_enemies_kill_us				
				

				; Check if enemy0 killed us
				; Only check if enemy0 is still alive
				LDR r4, =enemy0_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE player_killed_by_enemy1
				
				; Check if player_pos == enemy0_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy0_position
				LDR r1, [r4]
				CMP r0, r1
				BNE player_killed_by_enemy1
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE				; 0xE == 1110
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC				; 0xC == 1100
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8				; 0x8 == 1000
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0				; 0x0 == 0000
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				B reinitialize_game
				
player_killed_by_enemy1

				; Check if enemy1 killed us
				; Only check if enemy1 is still alive
				LDR r4, =enemy1_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE player_killed_by_enemy2
				
				; Check if player_pos == enemy1_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy1_position
				LDR r1, [r4]
				CMP r0, r1
				BNE player_killed_by_enemy2
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				B reinitialize_game
				
				
player_killed_by_enemy2

				; Check if enemy0 killed us
				; Only check if enemy2 is still alive
				LDR r4, =enemy2_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE are_all_enemies_dead

				; Check if player_pos == enemy0_pos
				LDR r4, =player_position
				LDR r0, [r4]
				LDR r4, =enemy2_position
				LDR r1, [r4]
				CMP r0, r1
				BNE are_all_enemies_dead
				
				; Set player_alive to 0 == player is dead
				LDR r4, =player_alive
				MOV r5, #0
				STRB r5, [r4]
				
				
				
				; Decrement player_lives_remaining by 1
				LDR r4, =player_lives_remaining
				LDR r2, [r4]
				SUB r2, r2, #1
				STR r2, [r4]
				
				; Changes LEDs based on number of lives left
				CMP r2, #3
				MOVEQ r0, #0xE
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #2
				MOVEQ r0, #0xC
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #1
				MOVEQ r0, #0x8
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
				
				CMP r2, #0
				MOVEQ r0, #0x0
				STMFDEQ SP!, {r0-r12, lr}
				BLEQ illuminateLEDs
				LDMFDEQ SP!, {r0-r12, lr}
			
				B reinitialize_game
				

; Used to reinitialize gameboard
; Resets player and enemy positions
reinitialize_game

				LDR r4, =player_position
				MOV r0, #0x20
				STRB r0, [r4]			; Overwrite player char with space at old position
				
				; Initialize player position to center of gameboard
				LDR r4, =player_position
				MOV r0, #208 ;
				STR r0, [r4]
				
				LDR r4, =gb_0
				MOV r1, #0x76 		; 'v' == 0x76 (initialize player to facing-south position)
				STRB r1, [r4, r0]	; Store 0x76 at gb_0 address + offset to center of gameboard (208)

				LDR r4, =player_direction	; Set player direction DOWN
				MOV r1, #22
				STR r1, [r4]
				
				; Reset enemies to their initial placements from this level
				
				LDR r4, =enemy0_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE redraw_enemy1
				
				; Enemy 0
				LDR r4, =enemy0_position
				LDR r1, [r4]
			
				LDR r4, =gb_0
				MOV r0, #0x20
				STRB r0, [r4, r1]
				
				LDR r4, =enemy0_position_init
				LDR r1, [r4]
				LDR r4, =enemy0_position
				STR r1, [r4]
				
				LDR r4, =gb_0
				MOV r0, #0x42
				STRB r0, [r4, r1]

redraw_enemy1	
				LDR r4, =enemy1_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE redraw_enemy2
				
				; Enemy 1
				LDR r4, =enemy1_position
				LDR r1, [r4]
			
				LDR r4, =gb_0
				MOV r0, #0x20
				STRB r0, [r4, r1]
				
				LDR r4, =enemy1_position_init
				LDR r1, [r4]
				LDR r4, =enemy1_position
				STR r1, [r4]
				
				LDR r4, =gb_0
				MOV r0, #0x62
				STRB r0, [r4, r1]

redraw_enemy2

				LDR r4, =enemy2_alive
				LDRB r0, [r4]
				CMP r0, #1
				BNE clear_timer0_interrupt
				
				;Enemy 2
				LDR r4, =enemy2_position
				LDR r1, [r4]
			
				LDR r4, =gb_0
				MOV r0, #0x20
				STRB r0, [r4, r1]
				
				LDR r4, =enemy2_position_init
				LDR r1, [r4]
				LDR r4, =enemy2_position
				STR r1, [r4]
				
				LDR r4, =gb_0
				MOV r0, #0x62
				STRB r0, [r4, r1]
				
				
				LDR r4, =player_position
				LDR r0, [r4]
				
				LDR r4, =gb_0
				MOV r1, #0x76 		; 'v' == 0x76 (initialize player to facing-south position)
				STRB r1, [r4, r0]	; Store 0x76 at gb_0 address + offset to center of gameboard (208)
				
				B clear_timer0_interrupt
				
	LTORG
				
; Are all enemies dead?
are_all_enemies_dead	

				MOV r1, #0			; Count of number of enemies alive
				LDR r4, =enemy0_alive
				LDRB r2, [r4]
				CMP r2, #1
				ADDEQ r1, r1, #1	; Incrememnt r1 if enemy0 alive
				
				LDR r4, =enemy1_alive
				LDRB r2, [r4]
				CMP r2, #1
				ADDEQ r1, r1, #1	; Incrememnt r1 if enemy1 alive
				
				LDR r4, =enemy2_alive
				LDRB r2, [r4]
				CMP r2, #1
				ADDEQ r1, r1, #1	; Incrememnt r1 if enemy2 alive
				
				; if r1 == 0, no enemies are alive, so we need to level up
				CMP r1, #0
				LDREQ r4, =level_up
				MOVEQ r2, #0x1
				STRBEQ r2, [r4]
				
				B clear_timer0_interrupt

clear_timer0_interrupt

				; Toggle slow_enemy_tick_flag (0 -> 1 ; 1 -> 0)
				LDR r4, =slow_enemy_tick_flag
				LDRB r0, [r4]
				EOR r0, r0, #1
				STRB r0, [r4]
				

				BL print_gameboard
				
				; If player_lives_remaining == 0, game is over, so set to 1
				LDR r4, =player_lives_remaining
				LDR r0, [r4]
				CMP r0, #0
				MOVEQ r1, #1
				LDREQ r4, =game_over
				STRBEQ r1, [r4]
				
game_over_end	LDMFD SP!, {r0-r12, lr}   ; Restore registers, TIMER0
				
				; Used to clear the TIMER0 interrupt
				; We clear interrupt by setting bit 1 to 1
				LDR r4, =T0IR
				LDR r1, [r4]
				ORR r1, r1, #2
				STR r1, [r4]
				
				

FIQ_Exit2

				LDMFD SP!, {r0-r12, lr}
				SUBS pc, lr, #4
	LTORG

			
; print_gameboard prints strings
; gb_0 ... gb_16 consecutively with a carriage return
; and new line printed in between each string.
; This displays the gameboard in PuTTY.
; screen_padding string is used to add three spaces in front of each gb_x string
; Number of walls encountered + SCORE_STRING are printed above gameboard
; Numeric score is converted to string by calling stringify_SCORE
print_gameboard
				STMFD SP!, {r0-r12, lr}
				
				; Print new page in PuTTY
				LDR r4, =FORM_FEED
				BL output_string
				
				LDR r4, =PAUSED
				LDRB r0, [r4]
				CMP r0, #1
				BNE print_score
				
				; Print "PAUSED" at top of screen
				LDR r4, =paused_string
				BL output_string
				BL enter
				
				B print_board
				
				
print_score				
				; Numeric score is converted to string
				BL stringify_SCORE
	
				LDR r4, =score_label
				BL output_string
				
				LDR r4, =SCORE_STRING
				BL output_string
				BL enter

print_board				
				
				; Print string gb_0, the top row of gameboard (border)
				LDR r4, =gb_0
				BL output_string
				BL enter
				
				
				LDR r4, =gb_1
				BL output_string
				BL enter
				
				
				LDR r4, =gb_2
				BL output_string
				BL enter
				
				
				LDR r4, =gb_3
				BL output_string
				BL enter
				
				
				LDR r4, =gb_4
				BL output_string
				BL enter
				
				
				LDR r4, =gb_5
				BL output_string
				BL enter
				
				
				LDR r4, =gb_6
				BL output_string
				BL enter
				
				
				LDR r4, =gb_7
				BL output_string
				BL enter
				
				
				LDR r4, =gb_8
				BL output_string
				BL enter
				
				
				LDR r4, =gb_9
				BL output_string
				BL enter
				
				
				LDR r4, =gb_10
				BL output_string
				BL enter
				
				
				LDR r4, =gb_11
				BL output_string
				BL enter
				
				
				LDR r4, =gb_12
				BL output_string
				BL enter
				
				
				LDR r4, =gb_13
				BL output_string
				BL enter
				
				
				LDR r4, =gb_14
				BL output_string
				BL enter
				
				
				LDR r4, =gb_15
				BL output_string
				BL enter
				
				; Print bottom border of gameboard
				
				LDR r4, =gb_16
				BL output_string
				BL enter
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
				
reset_gameboard_strings
				STMFD SP!, {r0-r12, lr}
				
				; Load each byte from gameboard_init strings (gb_0_init ... gb_16_init) and store at corresponding
				; byte positions in gameboard strings (gb_0 ... gb_16) to reset gameboard strings to default.
				
				MOV r2, #0xBB		; There are 374 bytes in the gameboard, including NULL terminators (has value: 0x00)
				MOV r2, r2, LSL #1
				
				LDR r0, =gb_0_init
				LDR r1, =gb_0
				MOV r3, #0			; Initialize loop counter to 0 ; Loop counter is also memory offset
reset_gameboard_loop
				CMP r2, r3			; Compare loop counter to Number of bytes total
				BEQ reset_gameboard_exit_loop
				
				LDRB r4, [r0, r3]
				STRB r4, [r1, r3]
				ADD r3, r3, #1		; Increment counter
				B reset_gameboard_loop
				
reset_gameboard_exit_loop				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
; This function returns a random integer between a and b inclusive in register r0
; Function arguments: a in r0
;					  b in r1
; Cases the function can accept: [0,1], [0,2], [0,3], [3,15], [1,19]
; Do NOT use this function if your [a,b] does NOT have one of the five above forms!
; Garbage will likely result!

rand_a_b		
				STMFD SP!, {r2-r12, lr}
				
				; Wrap offsets back to 0 when appropriate
				; For example, with the offset_1_bit, you can pass through rand_32 32 times (offsets 0 - 31), so reset when offset == 32
				LDR r4, =offset_1_bit
				LDR r2, [r4]
				CMP r2, #32
				BNE check_offset_2_bit
				
				; Here, offset_1_bit == 32, so set to 0
				MOV r2, #0
				STR r2, [r4]
				
				; If we have wrapped offset from 32 to 0, do the next operations in introduce more randomness
				; Take current rand_32 and AND it with current TC1 value and MVN result (essentially a NAND operation)
				LDR r4, =rand_32_bit
				LDR r2, [r4]
				
				LDR r4, =T1TC
				LDR r9, [r4]
				
				AND r2, r2, r9
				MVN r2, r2
				
				LDR r4, =rand_32_bit
				STR r2, [r4]
				
				
check_offset_2_bit
				LDR r4, =offset_2_bit
				LDR r2, [r4]
				CMP r2, #16
				BNE check_offset_4_bit
				
				; Here, offset_2_bit == 16, so set to 0
				MOV r2, #0
				STR r2, [r4]
				
				; Take current rand_32 and AND it with current TC1 value and MVN result (essentially a NAND operation)
				LDR r4, =rand_32_bit
				LDR r2, [r4]
				
				LDR r4, =T1TC
				LDR r9, [r4]
				
				AND r2, r2, r9
				MVN r2, r2
				
				LDR r4, =rand_32_bit
				STR r2, [r4]
				
check_offset_4_bit
				LDR r4, =offset_4_bit
				LDR r2, [r4]
				CMP r2, #8
				BNE check_offset_5_bit
				
				; Here, offset_4_bit == 8, so set to 0
				MOV r2, #0
				STR r2, [r4]
				
				; Take current rand_32 and AND it with current TC1 value and MVN result (essentially a NAND operation)
				LDR r4, =rand_32_bit
				LDR r2, [r4]
				
				LDR r4, =T1TC
				LDR r9, [r4]
				
				AND r2, r2, r9
				MVN r2, r2
				
				LDR r4, =rand_32_bit
				STR r2, [r4]
				
check_offset_5_bit
				LDR r4, =offset_5_bit
				LDR r2, [r4]
				CMP r2, #6
				BNE continue_rand
				
				; Here, offset_5_bit == 6, so set to 0
				MOV r2, #0
				STR r2, [r4]
				
				; Take current rand_32 and AND it with current TC1 value and MVN result (essentially a NAND operation)
				LDR r4, =rand_32_bit
				LDR r2, [r4]
				
				LDR r4, =T1TC
				LDR r9, [r4]
				
				AND r2, r2, r9
				MVN r2, r2
				
				LDR r4, =rand_32_bit
				STR r2, [r4]
				
				
continue_rand				
				; Check which case we are in -- for this, we simply check the value of b
				; r1 is b
				MOV r3, #1
				CMP r1, r3
				BNE check_2
				; If here, r1 == 1
				
				MOV r2, r0			; r2 := a (r2 is temp)
				
				
				LDR r4, =rand_32_bit
				LDR r7, [r4]		; value of rand_32_bit
				LDR r5, =offset_1_bit
				LDR r6, [r5]
				ADD r8, r6, #1		; increment offset by 1
				STR r8, [r5]		; store new offset back to memory
				RSB r3, r6, #31
				MOV r7, r7, LSL r3
				MOV r7, r7, LSR #31
				MOV r0, r7
				ADD r0, r0, r2
				
				B exit_rand_a_b

check_2			MOV r3, #2
				CMP r1, r3
				BNE check_3
				; If here, r1 == 2
				
				MOV r2, r0			; r2 := a (r2 is temp)
									; b := b - a + 1
				SUB r1, r1, r0		; b := b - a
				ADD r1, r1, #1		; b := b + 1
				
				LDR r4, =rand_32_bit
				LDR r3, [r4]
				LDR r5, =offset_2_bit
				LDR r6, [r5]
				ADD r7, r6, #1		; increment offset by 1
				STR r7, [r5]
									; r6 := (r6 * 2) + 1
				MOV r6, r6, LSL #1	; r6 := r6 * 2
				ADD r6, r6, #1		; r6 := r6 + 1
				
				RSB r6, r6, #31
				MOV r3, r3, LSL r6
				MOV r0, r3, LSR #30
				
				; r0 is our dividend
				; r1 is our divisor (3)
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				
				; Our modulus is in r0
				MOV r0, r1
				ADD r0, r0, r2		; r0 := modulus + a
				
				B exit_rand_a_b
				
check_3			MOV r3, #3
				CMP r1, r3
				BNE check_15
				; If here, r1 == 3
				
				LDR r4, =rand_32_bit
				LDR r3, [r4]
				LDR r5, =offset_2_bit
				LDR r6, [r5]
				ADD r7, r6, #1		; increment offset by 1
				STR r7, [r5]
									; r6 := (r6 * 2) + 1
				MOV r6, r6, LSL #1	; r6 := r6 * 2
				ADD r6, r6, #1		; r6 := r6 + 1
				
				RSB r6, r6, #31
				MOV r3, r3, LSL r6
				MOV r0, r3, LSR #30
				
				B exit_rand_a_b
				
check_15		MOV r3, #15
				CMP r1, r3
				BNE check_19
				; If here, r1 == 15
				
				MOV r2, r0			; r2 := a (r2 is temp)
									; b := b - a + 1
				SUB r1, r1, r0		; b := b - a
				ADD r1, r1, #1		; b := b + 1
				
				LDR r4, =rand_32_bit
				LDR r3, [r4]
				LDR r5, =offset_4_bit
				LDR r6, [r5]
				ADD r7, r6, #1		; increment offset by 1
				STR r7, [r5]
				
				MOV r6, r6, LSL #2	; r6 := r6 * 4
				ADD r6, r6, #3		; r6 := r6 + 3
				RSB r6, r6, #31
				MOV r3, r3, LSL r6
				MOV r0, r3, LSR #28
				
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				
				ADD r0, r1, r2			; r0 := modulus + a
				
				B exit_rand_a_b
				
check_19		; If here, r1 == 19

				MOV r2, r0			; r2 := a (r2 is temp)
				
				LDR r4, =rand_32_bit
				LDR r3, [r4]		; value of rand_32_bit
				LDR r5, =offset_5_bit
				LDR r6, [r5]
				ADD r9, r6, #1		; increment offset
				STR r9, [r5]		; Store incremented offset back to memory
				ADD r7, r6, r6, LSL #2		; b := 5n
				ADD r7, r7, #4				; b := b + 4
				RSB r7, r7, #31				; Mapping offset value to corresponding bit positions (highest bit position of offset)
											; Ex: offset(n) = 2, 5n + 4 := 14, bit positions are 14:11
				MOV r3, r3, LSL r7
				MOV r0, r3, LSR #27	; r0 is dividend for div_and_mod
									; r1 == b is divisor for div_and_mod
				; Do division of 5-bit contents of rand_32_bit / 19
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				; r1 is modulus, our random number
				ADD r0, r1, r2 		; Add 1 to modulus (modulus has range [0,18], we need to shift to [1,19])
				; r0 is our return value in range [1,19]
				B exit_rand_a_b
				

exit_rand_a_b
				LDMFD SP!, {r2-r12, lr}
				BX lr             	   ; Return

; Used to intialize random blank spots in map
; Used to initialize enemies to their starting positions
enemy_init
				STMFD SP!, {r0-r12, lr}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;ENEMY 0 INITIALIZATION;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

				; Random Row number
				MOV r0, #3
				MOV r1, #15
				BL rand_a_b
				MOV r2, r0		; Store rand [3,15] result in temp
				
				; Random Column Number
				MOV r0, #1
				MOV r1, #19
				BL rand_a_b
				
				MOV r3, r2, LSL #4		; Row random number * 22
				ADD r4, r2, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r2, r4, r3
				
				ADD r2, r2, r0			; Random position(r2) = Row * 22 + column
				
				; Gets blank random direction (0 = horiz, 1 = vert)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				MOV r3, r0
				MOV r6, r0
				
				; Gets enemy random direction (0 = left, 1 = right) (0 = up, 1 = down)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				
				; shift r3 left by 1 to get 2 bits
				; add r0 
				; 00 = horiz, left
				; 01 = horiz, right
				; 10 = vert, up
				; 11 = vert, down
				MOV r3, r3, LSL #1
				ADD r0 ,r0 ,r3
				
				; Set direction based on random number [0,3]
				LDR r4, =enemy0_direction
				CMP r0, #0
				MOVEQ r1, #-1
				
				CMP r0, #1
				MOVEQ r1, #1
				
				CMP r0, #2
				MOVEQ r1, #-22
				
				CMP r0, #3
				MOVEQ r1, #22
				
				STR r1, [r4]
				
				; Initial position cases (top, bottom, left, right, middle)
				; TOP CASE
				
				; If enemy position is 208, switch to 90
				CMP r2, #208
				MOVEQ r2, #90
				
				; r6 is a rand [0,1]
				; r6 == 0, we are initializing enemy with empty spaces to its left and right
				; r6 == 1, we are initializing enemy with empty spaces to its top and bottom
				CMP r6, #0
				BNE check_top0

; If (new position - 1) % 22 == 0, then we are along the left wall
check_left0		SUB r0, r2, #1
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE check_right0
				
				; Create enemy0 start position (blank spaces) and write enemy char in middle
				; Put space
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				; Put 'B' at new position
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init	; set position_init to new position
				STR r2, [r4]
				ADD r2, r2, #1
				
				; Put space in position to right of new position
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos0_init_done

; If (new position + 3) % 22 == 0, then we are along the right wall
check_right0	ADD r0, r2, #3
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE middle_case0
				
				SUB r2, r2, #2
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos0_init_done
				
check_top0		CMP r2, #85
				BGT check_bottom0
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos0_init_done
				
check_bottom0	MOV r0, #0x14
				MOV r0, r0, LSL #4
				ADD r0, r0, #0xB
				CMP r2, r0
				BLT middle_case0		; 0x14B == 331
				
				SUB r2, r2, #44
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				SUB r2, r2, #22
				
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init
				STR r2, [r4]
				SUB r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos0_init_done

; In the middle case, we are placing spaces to left and right of new enemy position
middle_case0		
				CMP r6, #0
				BNE middle_vert0
				
				SUB r2, r2, #1
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos0_init_done

; in middle vert case we place spaces to top and bottom of new enemy position
middle_vert0	SUB r2, r2, #22
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x42
				STRB r7, [r4,r2]
				LDR r4, =enemy0_position
				STR r2, [r4]
				LDR r4, =enemy0_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]

enemy_pos0_init_done

; See ENEMY 0 INITIALIZATION code for comments; they apply
; equally to ENEMY 1 and 2 code with the exception that
; we write a 'b' to memory instead of a 'B'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;ENEMY 1 INITIALIZATION;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Random Row number
				LDR r4, =enemy0_position_init
				LDR r10, [r4]
				
				MOV r0, #3
				MOV r1, #15
				BL rand_a_b
				MOV r2, r0		; Store rand [3,15] result in temp
				
				; Random Column Number
				MOV r0, #1
				MOV r1, #19
				BL rand_a_b
				
				MOV r3, r2, LSL #4		; Row random number * 22
				ADD r4, r2, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r2, r4, r3
				
				ADD r2, r2, r0			; Random position(r2) = Row * 22 + column
				CMP r2, r10
				BEQ enemy_pos0_init_done
				CMP r2, #208
				BEQ enemy_pos0_init_done
				
				
				; Gets blank random direction (0 = horiz, 1 = vert)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				MOV r3, r0
				MOV r6, r0
				
				; Gets enemy random direction (0 = left, 1 = right) (0 = up, 1 = down)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				
				; shift r3 left by 1 to get 2 bits
				; add r0 
				; 00 = horiz, left
				; 01 = horiz, right
				; 10 = vert, up
				; 11 = vert, down
				MOV r3, r3, LSL #1
				ADD r0 ,r0 ,r3
				
				LDR r4, =enemy1_direction
				CMP r0, #0
				MOVEQ r1, #-1
				
				CMP r0, #1
				MOVEQ r1, #1
				
				CMP r0, #2
				MOVEQ r1, #-22
				
				CMP r0, #3
				MOVEQ r1, #22
				
				STR r1, [r4]
				
				; Initial position cases (top, bottom, left, right, middle)
				; TOP CASE
				
				CMP r6, #0
				BNE check_top1
				
check_left1		SUB r0, r2, #1
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE check_right1
				
				; Create enemy0 start position (blank spaces) and write enemy char in middle
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos1_init_done

check_right1	ADD r0, r2, #3
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE middle_case1
				
				SUB r2, r2, #2
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos1_init_done
				
check_top1		CMP r2, #85
				BGT check_bottom1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos1_init_done
				
check_bottom1	MOV r0, #0x14
				MOV r0, r0, LSL #4
				ADD r0, r0, #0xB
				CMP r2, r0
				BLT middle_case1		; 0x14B == 331
				
				SUB r2, r2, #44
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				SUB r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				SUB r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos1_init_done
				
middle_case1		
				CMP r6, #0
				BNE middle_vert1
				
				SUB r2, r2, #1
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos1_init_done
				
middle_vert1	SUB r2, r2, #22
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy1_position
				STR r2, [r4]
				LDR r4, =enemy1_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]

enemy_pos1_init_done

; See ENEMY 0 INITIALIZATION code for comments; they apply
; equally to ENEMY 1 and 2 code with the exception that
; we write a 'b' to memory instead of a 'B'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;ENEMY 2 INITIALIZATION;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Random Row number
				MOV r0, #3
				MOV r1, #15
				BL rand_a_b
				MOV r2, r0		; Store rand [3,15] result in temp
				
				; Random Column Number
				MOV r0, #1
				MOV r1, #19
				BL rand_a_b
				
				MOV r3, r2, LSL #4		; Row random number * 22
				ADD r4, r2, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r4, r4, r2
				ADD r2, r4, r3
				
				ADD r2, r2, r0			; Random position(r2) = Row * 22 + column
				CMP r2, #208
				BEQ enemy_pos1_init_done
				
				LDR r4, =enemy0_position_init
				LDR r10, [r4]
				CMP r2, r10
				BEQ enemy_pos1_init_done
				
				LDR r4, =enemy1_position_init
				LDR r10, [r4]
				CMP r2, r10
				BEQ enemy_pos1_init_done
				
				
				; Gets blank random direction (0 = horiz, 1 = vert)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				MOV r3, r0
				MOV r6, r0
				
				; Gets enemy random direction (0 = left, 1 = right) (0 = up, 1 = down)
				MOV r0, #0
				MOV r1, #1
				BL rand_a_b
				
				; shift r3 left by 1 to get 2 bits
				; add r0 
				; 00 = horiz, left
				; 01 = horiz, right
				; 10 = vert, up
				; 11 = vert, down
				MOV r3, r3, LSL #1
				ADD r0 ,r0 ,r3
				
				LDR r4, =enemy2_direction
				CMP r0, #0
				MOVEQ r1, #-1
				
				CMP r0, #1
				MOVEQ r1, #1
				
				CMP r0, #2
				MOVEQ r1, #-22
				
				CMP r0, #3
				MOVEQ r1, #22
				
				STR r1, [r4]
				
				; Initial position cases (top, bottom, left, right, middle)
				; TOP CASE
				
				CMP r6, #0
				BNE check_top2
				
check_left2		SUB r0, r2, #1
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE check_right2
				
				; Create enemy0 start position (blank spaces) and write enemy char in middle
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos2_init_done

check_right2	ADD r0, r2, #3
				MOV r1, #22
				STMFD SP!, {r2-r12, lr}
				BL div_and_mod
				LDMFD SP!, {r2-r12, lr}
				CMP r1, #0
				BNE middle_case2
				
				SUB r2, r2, #2
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos2_init_done
				
check_top2		CMP r2, #85
				BGT check_bottom2
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos2_init_done
				
check_bottom2	MOV r0, #0x14
				MOV r0, r0, LSL #4
				ADD r0, r0, #0xB
				CMP r2, r0
				BLT middle_case2		; 0x14B == 331
				
				SUB r2, r2, #44
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				SUB r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				SUB r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos2_init_done
				
middle_case2		
				CMP r6, #0
				BNE middle_vert2
				
				SUB r2, r2, #1
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #1
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				ADD r2, r2, #1
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				
				B enemy_pos2_init_done
				
middle_vert2	SUB r2, r2, #22
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]
				ADD r2, r2, #22
				
				MOV r7, #0x62
				STRB r7, [r4,r2]
				LDR r4, =enemy2_position
				STR r2, [r4]
				LDR r4, =enemy2_position_init
				STR r2, [r4]
				ADD r2, r2, #22
				
				LDR r4, =gb_0
				MOV r7, #0x20
				STRB r7, [r4,r2]

enemy_pos2_init_done
				
				LDR r4, =enemy0_position_init
				LDR r10, [r4]
				MOV r0, #0x42
				LDR r4, =gb_0
				STRB r0, [r4,r10]
				
				LDR r4, =enemy1_position_init
				LDR r10, [r4]
				MOV r0, #0x62
				LDR r4, =gb_0
				STRB r0, [r4,r10]
				
				LDR r4, =enemy2_position_init
				LDR r10, [r4]
				MOV r0, #0x62
				LDR r4, =gb_0
				STRB r0, [r4,r10]
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return



interrupt_init       
				STMFD SP!, {r0-r1, lr}   ; Save registers 
				
				; Push button setup		 
				LDR r0, =0xE002C000
				LDR r1, [r0]
				ORR r1, r1, #0x20000000
				BIC r1, r1, #0x10000000
				STR r1, [r0]  ; PINSEL0 bits 29:28 = 10

				; Classify sources as IRQ or FIQ
				; 1 == FIQ, 0 == IRQ
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0xC]
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 interrupt
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				ORR r1, r1, #0x20	; Timer1 interrupt 
				STR r1, [r0, #0xC]

				; Enable Interrupts
				; 1 == enable, 0 == NO EFFECT
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x10] 
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt enable 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x10]

				;UART0 Interrupt When Data Recieved U0IER
				LDR r0, =0xE000C004		; Load U0EIR address
				LDR r1, [r0]			; Load U0EIR contents
				ORR r1, r1, #1			; Set bit 0 to 1 to enable Data recieved interrupt
				STR r1, [r0]			; Store contents back

				; External Interrupt 1 setup for edge sensitive
				LDR r0, =0xE01FC148
				LDR r1, [r0]
				ORR r1, r1, #2  ; EINT1 = Edge Sensitive
				STR r1, [r0]
				
				; Enable Timer0 to interrupt
				LDR r0, =T0MCR			; Load T0MCR address
				LDR r1, [r0]			; Load contents
				ORR r1, r1, #0x18		; Set bits 4:3 to 11 (0x18 == 0001 1000)
				STR r1, [r0]			; Store contents back

				; Enable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				; Bit 6 is for FIQ, Bit 7 is for IRQ
				MRS r0, CPSR
				BIC r0, r0, #0x40     ; Clear bit 6 to enable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0

				LDMFD SP!, {r0-r1, lr} ; Restore registers
				BX lr             	   ; Return
				
				
disable_interrupts
				
				STMFD SP!, {r0-r12, lr}
				
				; Disable Interrupts
				; 1 == disable interrupt
				; 0xFFFFF014 is the interrupt disable register address
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x14] 
				ORR r1, r1, #0x8000 ; Set bit to disable External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt disable
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				ORR r1, r1, #0x20	; Timer1 Disable
				STR r1, [r0, #0x14]
				
				; Disable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				MRS r0, CPSR
				ORR r0, r0, #0x40     ; Set bit 6 to disable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
		
			
; Converts numeric value in SCORE to string and places resulting string in SCORE_STRING
stringify_SCORE
				STMFD SP!, {r0-r12, lr}
				
				LDR r4, =SCORE
				LDR r2, [r4]		; r2 := value from SCORE
				LDR r4, =SCORE_STRING	; r4 := address of SCORE_STRING
				
				; Arguments are r2 and r4
				; r2: value to be converted to string
				; r4: address of string to be stored in memory
				BL make_chars_2
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
				
	END